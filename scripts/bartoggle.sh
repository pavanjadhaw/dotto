#!/bin/env bash

declare -A wm=([i3]=i3 [Fluxbox]=fb [Openbox]=ob [Herbstluftwm]=hlwm )

if [[ $(ps ax| grep "lemonbar" | grep -v "grep") ]];then
	killall lemonbar
else
	bash ~/dot/conf/lemonbar/${wm[$(wmctrl -m | grep "Name" | cut -d\  -f2)]}lemonbar.sh
fi
