#!/bin/bash

clock() {
    date +%H:%M:%S
}

battery() {
    cat /sys/class/power_supply/BAT1/capacity
}

Fcolor=#$(cat dot/col/moss | grep fore | cut -d# -f2)
Bcolor=#$(cat dot/col/moss | grep back | cut -d# -f2)

while true; do
    bar="%{B$Bcolor} %{F$Fcolor} %{c}LIFE : $(battery)%% TIME : $(clock)"
    echo $bar
    sleep 1
done
