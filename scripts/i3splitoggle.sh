#!/bin/bash

split="$HOME/dot/scripts/tmp/i3split"
current="$(cat $split)"

if [[ "$current" == "vertical" ]]; then
	echo "horizontal" > "$split"
else
	echo "vertical" > "$split"
fi

i3-msg split "$current" && notify-send "$current"
