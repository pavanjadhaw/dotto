"	    __ _  _  _  __  _  _  ____   ___
"	   (  ( \/ )( \(  )( \/ )(  _ \ / __)
"	 _ /    /\ \/ / )( /    \ )   /( (__
"	(_)\_)__) \__/ (__)\_)(_/(__\_) \___)
"	dheu

" dein {{{
if empty(glob('~/.local/dein/repos/github.com/Shougo/dein.vim'))
	:!curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > installer.sh
	:!sh ./installer.sh ~/.local/dein
	augroup dinstall
		autocmd!
		autocmd VimEnter * call dein#install | source $MYVIMRC
	augroup end

endif

if !empty(glob('~/.local/dein/repos/github.com/Shougo/dein.vim'))
	set runtimepath+=~/.local/dein/repos/github.com/Shougo/dein.vim
	if dein#load_state('~/.local/dein')
		call dein#begin('~/.local/dein')
		call dein#add('~/.local/dein')
		call dein#add('haya14busa/dein-command.vim')
		" call dein#add('autozimu/LanguageClient-neovim', {
					" \ 'rev': 'next',
					" \ 'build': './install.sh' })
		" call dein#add('w0rp/ale')

		call dein#add('Shougo/deoplete.nvim')
		call dein#add('roxma/nvim-yarp')
		call dein#add('roxma/vim-hug-neovim-rpc')
		call dein#add('zchee/deoplete-clang', { 'depends': 'deoplete'})
		call dein#add('zchee/deoplete-jedi', { 'depends': 'deoplete'})
		call dein#add('zchee/deoplete-go', {'build': 'make', 'depends': 'deoplete'})
		call dein#add('carlitux/deoplete-ternjs', {'build': 'npm install -g tern', 'depends': 'deoplete'})
		call dein#add('Shougo/neosnippet.vim')
		call dein#add('Shougo/neosnippet-snippets')

		call dein#add('rhysd/vim-clang-format')
		call dein#add('kana/vim-operator-user')
		call dein#add('vim-scripts/loremipsum')
		call dein#add('mattn/emmet-vim')
		call dein#add('mattn/webapi-vim')
		call dein#add('tpope/vim-speeddating')
		call dein#add('tpope/vim-repeat')
		call dein#add('machakann/vim-sandwich')
		call dein#add('tpope/vim-commentary')
		call dein#add('markonm/traces.vim')
		call dein#add('AndrewRadev/splitjoin.vim')
		call dein#add('wellle/targets.vim')
		call dein#add('joshdick/onedark.vim')
		call dein#add('vim-airline/vim-airline')
		call dein#add('vim-airline/vim-airline-themes')
		call dein#add('ap/vim-css-color')
		call dein#add('sheerun/vim-polyglot')
		call dein#add('junegunn/goyo.vim')
		" call dein#add('nathanaelkane/vim-indent-guides')

		call dein#add('honza/vim-snippets')
		call dein#add('iamcco/mathjax-support-for-mkdp')
		call dein#add('iamcco/markdown-preview.vim', { 'depends': 'mathjax-support-for-mkdp' })
		call dein#add('vim-pandoc/vim-pandoc')
		call dein#add('vim-pandoc/vim-pandoc-syntax')
		call dein#add('mhinz/vim-signify')

		call dein#add('scrooloose/nerdtree')
		call dein#add('Xuyuanp/nerdtree-git-plugin', { 'depends': 'nerdtree' })
		call dein#add('mbbill/undotree')

		call dein#add('rhysd/clever-f.vim')
		call dein#add('christoomey/vim-tmux-navigator')
		call dein#add('yuttie/comfortable-motion.vim')

		call dein#add('junegunn/fzf', { 'path': '/home/gwimm/.fzf', 'build': './install --all',  'merged': '0' })
		call dein#add('junegunn/fzf.vim', { 'depends': 'fzf' })
		call dein#add('urbainvaes/fzf-marks', { 'depends': 'fzf' })

		call dein#add('ryanoasis/vim-devicons')
		call dein#end()
		call dein#save_state()
	endif
endif
packloadall
silent! helptags ALL
" }}}
" plugin set {{{
" IndentGuide {{{
" let g:indent_guides_enable_on_vim_startup = 1
" let g:indent_guides_auto_colors = 0
" autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red   ctermbg=black
" autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=darkgrey
" let g:indent_guides_start_level=2
" let g:indent_guides_guide_size=1
" }}}
" language client {{{
let g:LanguageClient_serverCommands = {
	\ 'sh': ['bash-language-server', 'start']}
" }}}
" ale {{{
let g:ale_sign_error = '<>'
let g:ale_sign_warning = '>_'
" highlight clear ALEErrorSign
" highlight clear ALEWarningSign
highlight ALEWarning ctermbg=DarkMagenta
" let g:airline#extensions#ale#enabled = 1
let g:ale_fixers = {
			\ 'javascript': ['eslint'],
			\ }

let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

" function! LinterStatus() abort
	" let l:counts = ale#statusline#Count(bufnr(''))

	" let l:all_errors = l:counts.error + l:counts.style_error
	" let l:all_non_errors = l:counts.total - l:all_errors

	" return l:counts.total == 0 ? 'OK' : printf(
	" \   '%dW %dE',
	" \   all_non_errors,
	" \   all_errors
	" \)
" endfunction
" set statusline=%{LinterStatus()}
" }}}
" mkdp {{{
let g:mkdp_auto_start = 0
let g:mkdp_auto_open = 0
let g:mkdp_auto_close = 1
let g:mkdp_refresh_slow = 0
let g:mkdp_command_for_global = 0
" }}}
" pandoc {{{
let g:pandoc#filetypes#handled = ['pandoc', 'markdown']
" let g:pandoc#filetypes#pandoc_markdown = 0
let g:pandoc#modules#enabled = ['formatting', 'folding', 'bibliographies', 'completion', 'metadata', 'menu', 'keyboard', 'toc', 'chdir', 'spell', 'hypertext']
" }}}
" Goyo {{{
augroup goyo_enter
	autocmd!
	function! s:goyo_enter()
		set scl=no
	endfunction
	autocmd User GoyoEnter nested call <SID>goyo_enter()
augroup end
augroup goyo_leave
	autocmd!
	function! s:goyo_leave()
		set scl=auto
		source ~/dot/conf/nvim/killtilde.vim
	endfunction
	autocmd User GoyoLeave nested call <SID>goyo_leave()
augroup end
"}}}
" deoplete {{{
let g:deoplete#enable_at_startup = 1
let g:deoplete#auto_complete_delay = 20
" }}}
" neosnippet {{{
let g:neosnippet#enable_snipmate_compatibility = 1
" let g:neosnippet#snippets_directory='~/dot/conf/nvim/snips'
let g:neosnippet#snippets_directory = '~/.local/dein/repos/github.com/honza/vim-snippets/snippets'
" smap <C-I>     <Plug>(neosnippet_expand_or_jump)
imap <expr><C-S>
			\ (pumvisible() && neosnippet#expandable_or_jumpable()) ?
			\ "\<Plug>(neosnippet_expand_or_jump)" : "\<C-S>"
" }}}
" clang-format {{{
let g:clang_format#style_options = {
			\ 'AccessModifierOffset' : -4,
			\ 'AllowShortIfStatementsOnASingleLine' : 'true',
			\ 'AlwaysBreakTemplateDeclarations' : 'true',
			\ 'Standard' : 'C++11',
			\ 'BreakBeforeBraces' : 'Stroustrup'}
" }}}
" emmet {{{
let g:user_emmet_install_global = 1
let g:user_emmet_leader_key='<C-E>'
let g:user_emmet_settings = webapi#json#decode(join(readfile(expand('~/dot/conf/nvim/user_snippets.json')), "\n"))
"}}}
" airline {{{
let g:airline_highlighting_cache = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
" let g:airline_powerline_fonts = 1
let g:airline_theme = 'peaksea' "ayu_mirage, angr, badcat, bubblegum, dark_minimal, deus, monochrome, onedark, peaksea, raven, sol, solarized_flood, vice, wombat

if !exists('g:airline_symbols')
	let g:airline_symbols = {}
endif

" unicode symbols
" let g:airline_left_sep = '»'
" let g:airline_left_sep = '▶'
" let g:airline_right_sep = '«'
" let g:airline_right_sep = '◀'
" let g:airline_symbols.linenr = '☰'
" let g:airline_symbols.linenr = '␊'
" let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
" let g:airline_symbols.maxlinenr = ''
" let g:airline_symbols.maxlinenr = '㏑'
" let g:airline_symbols.branch = '⎇'
" let g:airline_symbols.paste = 'Þ'
" let g:airline_symbols.paste = '∥'

" powerline symbols
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.notexists = 'Ɇ'
let g:airline_symbols.whitespace = 'Ξ'
let g:airline_symbols.crypt = '🔒'
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.maxlinenr = ''
" }}}
" vim-signify {{{
let g:signify_realtime = 0
let g:signify_line_highlight = 0
"}}}
" comfortable-motion {{{
" let g:comfortable_motion_scroll_down_key = 'gj'
" let g:comfortable_motion_scroll_up_key = 'gk'
let g:comfortable_motion_friction = 80.0
let g:comfortable_motion_air_drag = 2.0
let g:comfortable_motion_no_default_key_mappings = 1
" }}}
" fzf {{{
let g:fzf_layout = { 'down': '~36%' }
augroup fzfstatusline
	autocmd!
	function! s:fzf_statusline()
		highlight fzf1 ctermfg=161 ctermbg=251
		highlight fzf2 ctermfg=23 ctermbg=251
		highlight fzf3 ctermfg=237 ctermbg=251
		setlocal statusline=%#fzf1#\ >\ %#fzf2#fz%#fzf3#f
	endfunction
	autocmd User FzfStatusLine call <SID>fzf_statusline()
augroup end
augroup diefzfstatusline
	autocmd!
	autocmd FileType fzf set laststatus=0 noshowmode noruler
		\ | autocmd BufLeave <buffer> set laststatus=2 showmode ruler
augroup end
"}}}
" }}}
" theme		{{{
if (has('autocmd') && !has('gui_running'))
	augroup colorset
		autocmd!
		let s:white = { 'gui': '#ABB2BF', 'cterm': '145', 'cterm16' : '7' }
		autocmd ColorScheme * call onedark#set_highlight('Normal', { 'fg': s:white })
	augroup end
endif
colorscheme onedark

source $VIMRUNTIME/syntax/syntax.vim
filetype plugin on
source ~/dot/conf/nvim/killtilde.vim
"}}}
" set	{{{
set clipboard+=unnamedplus
set pastetoggle=<F3>

set ignorecase

set tabstop=4 shiftwidth=4

set encoding=UTF-8
scriptencoding UTF-8

set undodir=~/.config/nvim/undodir undofile
set noshowmode

set nobackup nowritebackup noswapfile

set splitbelow splitright

set number relativenumber

set list
set listchars=tab:\▶\   

set cursorline
set foldmethod=marker
filetype indent on
"}}}
" autogroup	{{{
augroup group
	autocmd!

	" autocmd BufRead,BufNewFile * set scl=no
	autocmd StdinReadPre * let s:std_in=1
	autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
	autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

	autocmd BufRead,BufNewFile /tmp/calcurse* set filetype=markdown
	autocmd BufRead,BufNewFile ~/.calcurse/notes/* set filetype=markdown

	autocmd BufNewFile,BufRead \*.{md,mdwn,mkd,mkdn,mark\*} set filetype=markdown

	autocmd BufRead,BufNewFile ~/doc/* set filetype=markdown
	autocmd BufRead,BufNewFile ~/dot/col/* set filetype=xdefaults

	autocmd BufRead,BufNewFile */dheu* set filetype=markdown

	autocmd BufRead,BufNewFile ~/doc/log/* Goyo
augroup end
"}}}
" map	{{{
nnoremap <silent> gb	:bnext<CR>
nnoremap <silent> gB	:bprevious<CR>

" nnoremap <C-J>			<C-W><C-J>
" nnoremap <C-K>			<C-W><C-K>
" nnoremap <C-L>			<C-W><C-L>
" nnoremap <C-H>			<C-W><C-H>

nnoremap Y				y$

nnoremap k				gk
vnoremap k				gk
nnoremap j				gj
vnoremap j				gj

function! Multiply()
	if v:count!=0
		let @r=v:count*expand('<cword>') | normal diw"rP
	endif
endfunction

nnoremap <silent><C-m> :<C-u>call Multiply()<cr>
"}}}
" leader mappings {{{
let mapleader ='<'
let maplocalleader = ';'

nnoremap <silent> <leader>re :source $MYVIMRC<CR>
nnoremap <silent> <leader><space> :nohlsearch<CR>

nnoremap <leader>q :q<CR>
nnoremap <leader>Q :q!<CR>

vnoremap <silent> <leader>ma yo<Esc>p^y$V:!perl -e '$x = <C-R>"; print $x'<CR>-y0j0P

nnoremap <silent> <leader>c :set scl=no<CR>
nnoremap <silent> <leader>o :set scl=auto<CR>

nnoremap <Tab> za
"}}}
" plugin map {{{
nnoremap <silent> <leader>zf :FZF<CR>
nnoremap <silent> <leader>zb :Buffers<CR>

nnoremap <silent> <leader>mdo :MarkdownPreview<CR>
nnoremap <silent> <leader>mdc :MarkdownPreviewStop<CR>

nnoremap <silent> <leader>tf :NERDTreeToggle<CR>
nnoremap <silent> <leader>tu :UndotreeToggle<CR>:UndotreeFocus<CR>

nnoremap <silent> <leader>f :Goyo<CR><Esc>

nnoremap <silent>			<C-n> :call comfortable_motion#flick(100)<CR>
nnoremap <silent>			<C-p> :call comfortable_motion#flick(-100)<CR>
vnoremap <silent>			<C-n> :call comfortable_motion#flick(100)<CR>
vnoremap <silent>			<C-p> :call comfortable_motion#flick(-100)<CR>

augroup clang_format
	autocmd!
	autocmd FileType c,cpp,objc nnoremap gc :<C-u>ClangFormat<CR>
	autocmd FileType c,cpp,objc vnoremap gc :ClangFormat<CR>
augroup end
nmap <Leader>C :ClangFormatAutoToggle<CR>
" }}}
