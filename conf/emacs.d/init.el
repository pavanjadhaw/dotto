;;; package --- Summary
;;; use-package, zerodark-theme, beacon, which-key, spaceline, org-bullets, avy, company, irony, flycheck, ido-vertical-mode, smex,
;;; Commentary:
;;;
;;; Code:
(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(("ELPA"  . "http://tromey.com/elpa/")
			 ("gnu"   . "http://elpa.gnu.org/packages/")
			 ("melpa" . "https://melpa.org/packages/")
			 ("org"   . "https://orgmode.org/elpa/")))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(when (file-readable-p "~/dot/conf/emacs.d/config.org")
  (org-babel-load-file (expand-file-name "~/dot/conf/emacs.d/config.org")))

(custom-set-variables
 '(package-selected-packages (quote (zerodark-theme use-package))))

(custom-set-faces
 '(default ((t (:inherit nil :stipple nil :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 116 :width normal :foundry "1ASC" :family "Iosevka Term")))))
;;; init.el ends here
