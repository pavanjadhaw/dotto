#!/bin/bash

clock() {
	date +%H:%M:%S
}

battery() {
	cat /sys/class/power_supply/BAT1/capacity
}

Fcolor=#$(cat dot/col/moss | grep fore | cut -d# -f2)
Bcolor=#$(cat dot/col/moss | grep back | cut -d# -f2)

bar() {
	while true; do
		echo "%{B$Bcolor}%{F$Fcolor}%{l}%{O10}LIFE : $(battery)% TIME : $(clock)"
		sleep 1
	done
}
echo "" | lemonbar -g 1x43+0+0 -f "scientifica:bold" -F "#00000000" -B "#00FFFFFF" -p &
echo "" | lemonbar -g 1886x32+17+10 -B "$Fcolor" -d -p &
bar | lemonbar -g 1882x28+19+12 -f "scientifica:bold" -F "#FFFFFF" -B "#000000" -d -p &
