[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

# Just a few Dotfiles :P

_use these only if you know what you're doing..._

I try and keep all wm's nicely organized

wms

- [x] i3
- [x] herbstluftwm
- [] bspwm
- [] fluxbox
- [] openbox
